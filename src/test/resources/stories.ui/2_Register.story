Meta: @skip
Narrative:
    This story covers basic tests of Register Page

Lifecycle:
Before:
Given I open register url

Scenario: Open register url and check that all components have been loaded
Then Check loaded register components

Scenario: Check navbar login link
When Click on login link on the navbar

Scenario: Check login link under the register form
When Click on login link under the login form