Meta: @skip
Narrative:
    This story covers basic tests of Login Page

Lifecycle:
Before:
Given I open login url

Scenario: Open login url and check that all components have been loaded
Then Check loaded login components

Scenario: Check navbar register link
When Click on register link on the navbar

Scenario: Check register link under the login form
When Click on register link under the login form

Scenario: Register new user
When Click on register link on the navbar
When Type email in the email field register page
When Type password in the password field register page
When Click on Sign Up button register page
Then Should be redirected to the "https://mighty-garden-74991.herokuapp.com/api/auth/login/"

Scenario: Login with “mock” user
When Type email in the email field login page
When Type password in the password field login page
When Click on Sign In button login page


