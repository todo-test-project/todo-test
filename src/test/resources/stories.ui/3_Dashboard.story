Meta: @skip
Narrative:
    This story covers basic tests of Dashboard Page

Lifecycle:
Before:
Scope: STEP
Given I sign in to dashboard
Given Get user token

Scenario: Open dashboard url and check that all components have been loaded
Then Check loaded dashboard components

Scenario: Sign out from the dashboard page
When Click on sign out button on the navbar
Then Should be redirected to the "https://mighty-garden-74991.herokuapp.com/api/auth/login"

Scenario: Create a new card in the dashboard
When Click to +New button
Then Should display a new card
Given Remove all cards

Scenario: Add new task into new created card
When Click to +New button
Then Should display a new card
When Add new task "newTask"
Then Should display a "newTask"
Given Remove all cards

Scenario: Change task status in new created card
When Click to +New button
Then Should display a new card
When Add new task "newTask"
Then Should display a "newTask"
When Click on task in new created task "newTask"
Then Should be changed status for done in "newTask"
Given Remove all cards

Scenario: Rename title of new created card
When Click to +New button
Then Should display a new card
When Double click on card title
And Change card title to "renamedCard"
Then Title card should be changed to "renamedCard"
Given Remove all cards

Scenario: Rename any task in new created card
When Click to +New button
Then Should display a new card
When Add new task "newTask"
Then Should display a "newTask"
When Double click on task "newTask" in the card
And Change the task to "renamedTask" task
Then Task should be renamed to "renamedTask"
Given Remove all cards

Scenario: Remove new created card
When Click to +New button
Then Should display a new card
When Double click on card title
And Change card title to "CardShouldBeDeleted"
Then Title card should be changed to "CardShouldBeDeleted"
When Hover cursor to the title on the card "CardShouldBeDeleted"
And Click to the brush icon "CardShouldBeDeleted" card
Then Card "CardShouldBeDeleted" should be not displayed
Given Remove all cards

Scenario: Remove new created todo task
When Click to +New button
Then Should display a new card
When Add new task "newTask"
Then Should display a "newTask"
When Double click on task "newTask" in the card
And Change the task to "TaskShouldBeRemoved" task
Then Task should be renamed to "TaskShouldBeRemoved"
When Hover cursor on the task "TaskShouldBeRemoved"
And Click to the brush icon "TaskShouldBeRemoved" task
Then Task "TaskShouldBeRemoved" should be not displayed
Given Remove all cards

Scenario: Load created cards with tasks
When Click to +New button
Then Should display a new card
When Double click on card title
When Change card title to "renamedCard1"
Then Title card should be changed to "renamedCard1"
When Add new task "newTask"
Then Should display a "newTask"
When Click on sign out button on the navbar
Then Should be redirected to the "https://mighty-garden-74991.herokuapp.com/api/auth/login"
Given I sign in to dashboard
Given Get user token
Then Title card should be changed to "RENAMEDCARD1"
Then Should display a "newTask"
Given Remove all cards