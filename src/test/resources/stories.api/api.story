Meta:
@tag product: ToDo Rest API

Narrative:
    This story covers basic tests of API ToDo web-application

Scenario: Register new user from mock data
!-- 302 -> redirect
When Send post-request for "register" with mock username and mock password
Then Should be response code "302"

Scenario: Login new user from mock data
!-- 302 -> redirect
When Send post-request for "login" with mock username and mock password
Then Should be response code "302"

Scenario: Create new card
!-- 201 -> created
When Create new card "newcard"
Then Should be response code "201"

Scenario: Create todo list in new card
!-- 201 -> created
When Create new card "newcard"
Then Should be response code "201"
When Create task "task1"
Then Should be response code "201"
When Create task "task2"
Then Should be response code "201"

Scenario: Delete task from new created card
!-- 201 -> created

When Create new card "newcard"
Then Should be response code "201"
When Create task "task1"
Then Should be response code "201"
When Remove task "task1"
Then Should be response code "201"

Scenario: Delete new created card
!-- 201 -> created
When Create new card "newcard"
Then Should be response code "201"
When Remove card "newcard"
Then Should be response code "201"
