package ui.steps;

import io.restassured.response.Response;
import io.vavr.collection.Array;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Managed;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import ui.pages.DashboardPage;
import ui.pages.LoginPage;

import java.util.List;

@RunWith(SerenityRunner.class)
public class DashboardPageSteps {

    @Managed
    WebDriver driver;

    DashboardPage dashboardPage;
    LoginPage loginPage;

    private String email;
    private String password;
    private Response response;

    @Given("I sign in to dashboard")
    public void openDashboardUrl() {
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        loginPage.open();
        email = BeforeAndAfterSteps.mockEmail;
        password = BeforeAndAfterSteps.mockPassword;
        loginPage.typeEmail(email);
        loginPage.typePassword(password);
        loginPage.clickSignInBtn();
    }

    @Given("Get user token")
    public String getUserToken() {
        String currentUrl = driver.getCurrentUrl();
        String[] token =  currentUrl.split("/");
        return token[5];
    }

    @Given("Remove all cards")
    public void removeAllCards() {
        String allCardsUrl = "https://mighty-garden-74991.herokuapp.com/api/cards/";

        response = SerenityRest
                .given()
                .accept("application/json;charset=utf-8")
                .when()
                .get("https://mighty-garden-74991.herokuapp.com/api/cards/" + getUserToken());
        String cards = response.then().extract().jsonPath().getString("_id");
        cards = cards.substring(1);
        cards = cards.substring(0, cards.length()-1);
        String[] cardIds = cards.split(",");


        for(int i=0; i < cardIds.length; i++) {
            cardIds[i] = cardIds[i].trim();
            System.out.println("Card ID: " + cardIds[i]);
            SerenityRest.delete(allCardsUrl + cardIds[i]);
            System.out.println("Card was removed");
        }
    }


    @Then("Check loaded dashboard components")
    public void checkLoadedDashboardComponents() {
        Assert.assertEquals(dashboardPage.getNavbarHeaderText(), "To do");
        Assert.assertEquals(dashboardPage.getNavbarSignOutText(), "Sign out");
        Assert.assertTrue(dashboardPage.isNewCardBtnDisplayed());
    }

    @Then("Card \"$CardShouldBeDeleted\" should be not displayed")
    public void isCardRemoved(String cardName) throws InterruptedException {
        Assert.assertFalse(dashboardPage.isCardPresent(cardName));
    }

    @Then("Task \"$TaskShouldBeRemoved\" should be not displayed")
    public void isTaskRemoved(String taskName) throws InterruptedException {
        Assert.assertFalse(dashboardPage.isTaskPresent(taskName));
    }


    @Then("Should display a new card")
    public void isNewCardDisplayed() throws InterruptedException {
        Assert.assertTrue(dashboardPage.isNewCardDisplayed());
    }

    @Then("Should display a \"$newTask\"")
    public void isNewTaskDisplayed(String newTask) {
        Assert.assertTrue(dashboardPage.isNewTaskDisplayed(newTask));
    }

    @Then("Should be changed status for done in \"$newTask\"")
    public void isNewTaskChangedStatus(String newTask) {
        Assert.assertTrue(dashboardPage.isNewTaskChangedStatus(newTask));
    }

    @Then("Title card should be changed to \"$renamedCard\"")
    public void isNewCardTitleDisplayed(String renamedCard) {
        Assert.assertTrue(dashboardPage.isNewCardTitleDisplayed(renamedCard));
    }

    @Then("Task should be renamed to \"$renamedTask\"")
    public void isCurrentTaskRenamed(String renamedTask) {
        Assert.assertTrue(dashboardPage.isCurrentTaskRenamed(renamedTask));
    }

    @When("Click to +New button")
    public void clickOnNewCardBtn() {
        dashboardPage.clickOnNewCardBtn();
    }

    @When("Add new task \"$newTask\"")
    public void typeNewTaskAndConfirm(String newTask) {
        dashboardPage.typeNewTaskAndConfirm(newTask);
    }

    @When("Click on task in new created task \"$newTask\"")
    public void changeTaskStatus(String newTask) {
        dashboardPage.changeTaskStatus(newTask);
    }

    @When("Click on sign out button on the navbar")
    public void clickOnNavbarSignOut() {
        dashboardPage.clickOnNavbarSignOut();
    }

    @When("Double click on card title")
    public void doubleClickOnCardTitle() throws InterruptedException{
        dashboardPage.doubleClickOnCardTitle(driver);
    }

    @When("Double click on task \"$newTask\" in the card")
    public void doubleCLickOnTask(String newTask) {
        dashboardPage.doubleClickOnTask(driver, newTask);
    }

    @When("Change the task to \"$renamedTask\" task")
    public void typeTask(String renamedTask) {
        dashboardPage.typeTask(renamedTask);
    }


    @When("Change card title to \"$renamedCard\"")
    public void typeCardTitle(String renamedCard) {
        dashboardPage.typeCardTitle(renamedCard);
    }

    @When("Hover cursor to the title on the card \"$CardShouldBeDeleted\"")
    public void hoverOnCardTitle(String cardName) {
        dashboardPage.hoverOnCardTitle(cardName, driver);
    }

    @When("Hover cursor on the task \"$TaskShouldBeRemoved\"")
    public void hoverOnTask(String taskName) {
        dashboardPage.hoverOnTask(taskName);
    }

    @When("Click to the brush icon \"$TaskShouldBeRemoved\" task")
    public void removeTask(String taskName) throws InterruptedException {
        dashboardPage.removeTask(taskName);
    }

    @When("Click to the brush icon \"$CardShouldBeDeleted\" card")
    public void removeCard(String cardName) throws InterruptedException {
        dashboardPage.removeCard(cardName);
    }

}
