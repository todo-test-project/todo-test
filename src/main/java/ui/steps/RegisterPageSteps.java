package ui.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import ui.pages.RegisterPage;

public class RegisterPageSteps {

    private String mockEmail = BeforeAndAfterSteps.mockEmail;
    private String mockPassword = BeforeAndAfterSteps.mockPassword;
    RegisterPage registerPage;

    @Given("I open register url")
    public void openRegisterUrl() {
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        registerPage.open();
    }

    @Then("Check loaded register components")
    public void checkLoadedLoginComponents() {
        Assert.assertEquals(registerPage.getNavbarHeaderText(), "To do");
        Assert.assertEquals(registerPage.getNavbarLoginText(), "Login");
        Assert.assertEquals(registerPage.getSignInLinkText(), "Just Sign In!");
        Assert.assertTrue(registerPage.isEmailFieldDisplayed());
        Assert.assertTrue(registerPage.isPasswordFieldDisplayed());
        Assert.assertTrue(registerPage.isSignUpBtnDisplayed());
    }

    @When("Click on login link on the navbar")
    public void clickOnNavbarLogin() {
        registerPage.clickOnNavbarLogin();
    }

    @When("Click on login link under the login form")
    public void clickOnSignInLink() {
        registerPage.clickOnSignInLink();
    }

    @When("Type email in the email field register page")
    public void typeEmail() {
        registerPage.typeEmail(mockEmail);
    }

    @When("Type password in the password field register page")
    public void typePassword() {
        registerPage.typePassword(mockPassword);
    }

    @When("Click on Sign Up button register page")
    public void clickSignUpBtn() {
        registerPage.clickOnSignUpBtn();
    }
}
