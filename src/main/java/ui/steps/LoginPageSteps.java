package ui.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import ui.pages.DashboardPage;
import ui.pages.LoginPage;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

//@RunWith(SerenityRunner.class)
public class LoginPageSteps {

    private String mockEmail = BeforeAndAfterSteps.mockEmail;
    private String mockPassword = BeforeAndAfterSteps.mockPassword;
    LoginPage loginPage;
    DashboardPage dashboardPage;

    @Given("I open login url")
    public void openLoginUrl() {
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        loginPage.open();
    }

    @Then("Check loaded login components")
    public void checkLoadedLoginComponents() {

        Assert.assertEquals(loginPage.getNavbarHeaderText(), "To do");
        Assert.assertEquals(loginPage.getNavbarRegisterText(), "Register");
        Assert.assertEquals(loginPage.getSignUpLinkText(), "Just Sign Up!");
        Assert.assertTrue(loginPage.isEmailFieldDisplayed());
        Assert.assertTrue(loginPage.isPasswordFieldDisplayed());
        Assert.assertTrue(loginPage.isSignInBtnDisplayed());
    }

    @Then("Should be redirected to the \"$url\"")
    public void shouldBeRedirected(String url) {
        WebDriver driver = getDriver();
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals(url, currentUrl);
    }

    @When("Click on register link on the navbar")
    public void clickOnNavbarRegisterLink() {
        loginPage.clickToNavbarRegister();
    }

    @When("Click on register link under the login form")
    public void clickOnRegisterLinkUnderTheForm() {
        loginPage.clickToSignUpLink();
    }

    @When("Type email in the email field login page")
    public void typeEmail() {
        loginPage.typeEmail(mockEmail);
    }

    @When("Type password in the password field login page")
    public void typePassword() {
        loginPage.typePassword(mockPassword);
    }

    @When("Click on Sign In button login page")
    public void clickSignInBtn() {
        loginPage.clickSignInBtn();
    }

}
