package ui.steps;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.pages.PageObject;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BeforeAndAfterSteps extends PageObject {

    public static String mockEmail;
    public static String mockPassword;

    private Response response;
    private String currentUrl = null;

    private DashboardPageSteps dashboardPageSteps;

    @BeforeStories
    public void beforeStories() {
        mockEmail = "";
        Pattern p = Pattern.compile("\\d+");
        String pattern1 = "MM/dd/yyyy";
        String pattern2 = "HH:mm:ss";
        DateFormat df1 = new SimpleDateFormat(pattern1);
        DateFormat df2 = new SimpleDateFormat(pattern2);
        Date today = Calendar.getInstance().getTime();
        String email = df1.format(today) + df2.format(today);
        Matcher m = p.matcher(email);
        while (m.find()) mockEmail += m.group();
        mockEmail += "@test.com";
        mockPassword = "testpass";
        System.out.println(mockEmail);
    }

    @AfterStories
    public void afterStories() {
        response = SerenityRest
                .given()
                .accept("application/json;charset=utf-8")
                .when()
                .get("https://mighty-garden-74991.herokuapp.com/api/cards/5ed139d08d08ad002439ee9a/");
        System.out.println(response.getBody().asString());
    }
}
