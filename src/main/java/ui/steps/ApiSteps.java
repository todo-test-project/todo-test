package ui.steps;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.config.EncoderConfig.encoderConfig;

public class ApiSteps {

    private Response response;

    private Map<String, String> users = new HashMap<>();
    private Map<String, String> cards = new HashMap<>();
    private Map<String, String> tasks = new HashMap<>();

    private String userToken = null;
    private String cardId = null;
    private String taskId = null;

    private String baseUrl = "https://mighty-garden-74991.herokuapp.com/api/";

    private static final String CONTENT_TYPE = "application/json;charset=utf-8";

    @Step("Do action with new user with Mock Data")
    public void actionWithMockUser(String action ,String name, String pass) {
        users.put("email", name);
        users.put("password", pass);

        response = SerenityRest
                .given()
                .config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs(CONTENT_TYPE, ContentType.TEXT)))
                .accept(CONTENT_TYPE)
                .contentType(CONTENT_TYPE)
                .body(users).when().post(baseUrl + "auth/" + action);
        //Take user token from URL [In headers location value]
        if (action.equals("login")) {
            List<String> location = response.getHeaders().getValues("Location");
            String[] devidedString = location.get(0).split("/");
            userToken = devidedString[2];
        }
    }

    @Step("Creating new card")
    public void createNewCard(String cardname) {
        cards.put("cardTitle", cardname);
        cards.put("userId", userToken);

        response = SerenityRest
                .given().config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs(CONTENT_TYPE, ContentType.TEXT)))
                .accept(CONTENT_TYPE)
                .contentType(CONTENT_TYPE)
                .body(cards).when().post(baseUrl + "cards/add");

        cardId = response.jsonPath().getString("_id");
    }

    @Step("Create new task")
    public void createNewTask(String taskname) {
        tasks.put("fieldName", taskname);
        tasks.put("cardId", cardId);

        response = SerenityRest
                .given().config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs(CONTENT_TYPE, ContentType.TEXT)))
                .accept(CONTENT_TYPE)
                .contentType(CONTENT_TYPE)
                .body(tasks).when().post(baseUrl + "fields/add");

        taskId = response.jsonPath().getString("_id");
    }

    @Step("Remove task")
    public void removeTask() {
        SerenityRest.delete(baseUrl + "fields/" + taskId);
    }

    @Step("Remove card")
    public void removeCard() {
        SerenityRest.delete(baseUrl + "cards/" + cardId);
    }


    @Step("Check successful response")
    public void successfulResponse(int code) {
        response.then().statusCode(code);
    }

}
