package ui.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@DefaultUrl("https://mighty-garden-74991.herokuapp.com/api/auth/register")
public class RegisterPage extends PageObject {

    private By navbarHeader = By.xpath("//a[@class='navbar-brand']");
    private By navbarLogin = By.xpath("//a[@class='nav-link']");
    private By emailField = By.xpath("//input[@name='email']");
    private By passwordField = By.xpath("//input[@name='password']");
    private By signUpBtn = By.xpath("//button[@id='login-btn']");
    private By signInLink = By.xpath("//a[text()='Just Sign In!']");

    public RegisterPage() {
        setDefaultBaseUrl("https://mighty-garden-74991.herokuapp.com/api/auth/register");
    }

    public String getNavbarHeaderText() {
        return find(navbarHeader).getText();
    }
    public String getNavbarLoginText() {
        return find(navbarLogin).getText();
    }
    public boolean isEmailFieldDisplayed() {
        return find(emailField).isDisplayed();
    }
    public boolean isPasswordFieldDisplayed() {
        return find(passwordField).isDisplayed();
    }
    public boolean isSignUpBtnDisplayed() {
        return find(signUpBtn).isDisplayed();
    }
    public String getSignInLinkText() {
        return find(signInLink).getText();
    }

    public LoginPage clickOnNavbarLogin() {
        find(navbarLogin).click();
        return new LoginPage();
    }

    public LoginPage clickOnSignInLink() {
        find(signInLink).click();
        return new LoginPage();
    }

    public RegisterPage typeEmail(String email) {
        find(emailField).sendKeys(email);
        return this;
    }

    public RegisterPage typePassword(String pass) {
        find(passwordField).sendKeys(pass);
        return this;
    }

    public LoginPage clickOnSignUpBtn() {
        find(signUpBtn).click();
        return new LoginPage();
    }
}
