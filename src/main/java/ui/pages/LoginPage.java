package ui.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.DefaultUrl;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


@DefaultUrl("https://mighty-garden-74991.herokuapp.com/api/auth/login")
public class LoginPage extends PageObject {


    private By navbarHeader = By.xpath("//a[@class='navbar-brand']");
    private By navbarRegister = By.xpath("//a[@class='nav-link']");
    private By emailField = By.xpath("//input[@name='email']");
    private By passwordField = By.xpath("//input[@name='password']");
    private By signInBtn = By.xpath("//button[@id='login-btn']");
    private By signUpLink = By.xpath("//a[text()='Just Sign Up!']");

//    private static String token;

    public LoginPage() {
        setDefaultBaseUrl("https://mighty-garden-74991.herokuapp.com/api/auth/login/");
    }

    public String getNavbarHeaderText() {
        return find(navbarHeader).getText();
    }

    public LoginPage clickToNavbarHeader() {
        find(navbarHeader).click();
        return this;
    }

    public String getNavbarRegisterText() {
        return find(navbarRegister).getText();
    }

    public RegisterPage clickToNavbarRegister() {
        find(navbarRegister).click();
        return new RegisterPage();
    }

    public boolean isEmailFieldDisplayed() {
        return find(emailField).isDisplayed();
    }

    public LoginPage typeEmail(String email) {
        find(emailField).sendKeys(email);
        return this;
    }

    public boolean isPasswordFieldDisplayed() {
        return find(passwordField).isDisplayed();
    }

    public LoginPage typePassword(String pass) {
        find(passwordField).sendKeys(pass);
        return this;
    }

    public boolean isSignInBtnDisplayed() {
        return find(signInBtn).isDisplayed();
    }

    public DashboardPage clickSignInBtn() {
        find(signInBtn).click();
        // If entered valid data then should use mock data
        WebDriver driver = getDriver();
        String currentUrl = driver.getCurrentUrl();
        int statusCode = SerenityRest.get(currentUrl).statusCode();
        if (statusCode == 200) {
            openUrl("https://mighty-garden-74991.herokuapp.com/api/auth/login/");
            find(emailField).clear();
            find(emailField).sendKeys("test14062020@test.com");
            find(passwordField).clear();
            find(passwordField).sendKeys("testpass");
            find(signInBtn).click();
            return new DashboardPage();
        }
        return new DashboardPage();
    }

    public String getSignUpLinkText() {
        return find(signUpLink).getText();
    }

    public RegisterPage clickToSignUpLink() {
        find(signUpLink).click();
        return new RegisterPage();
    }
}
