package ui.pages;

import ui.steps.ApiSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import ui.steps.BeforeAndAfterSteps;

public class ApiTests {

    private String mockEmail = BeforeAndAfterSteps.mockEmail;
    private String mockPassword = BeforeAndAfterSteps.mockPassword;

    @Steps
    ApiSteps apiSteps;

    @When("Send post-request for \"$action\" with mock username and mock password")
    public void actionWithMockUser(String action) {
        apiSteps.actionWithMockUser(action, mockEmail, mockPassword);
    }

    @When("Create new card \"$newcard\"")
    public void createNewCard(String newcard) {
        apiSteps.createNewCard(newcard);
    }

    @When("Create task \"$taskname\"")
    public void createNewTask(String taskname) { apiSteps.createNewTask(taskname);}

    @When("Remove task \"task1\"")
    public void removeTask() { apiSteps.removeTask();}

    @When("Remove card \"newcard\"")
    public void removeCard() { apiSteps.removeCard(); }

    @Then("Should be response code \"$code\"")
    public void foundResponse(int code)  {
        apiSteps.successfulResponse(code);
    }

}
