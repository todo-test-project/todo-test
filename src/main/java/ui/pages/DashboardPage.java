package ui.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import ui.steps.DashboardPageSteps;

@DefaultUrl("https://mighty-garden-74991.herokuapp.com/api/login")
public class DashboardPage extends PageObject {

    private By navbarHeader = By.xpath("//a[@class='navbar-brand']");
    private By navbarSignOut = By.xpath("//a[@class='nav-link']");
    private By newCardBtn = By.xpath("//button[@id='newCard']");

    private By card = By.xpath("//div[@id='container']/div");
    private By inputTask = By.xpath("//input[@class='target']");
    private String task = "//li[text()='%s']";
    private String doneTask =  "//li[text()='%s' and @class='done']";
    private By cardTitleElement = card.xpath("//div[@class='title']");
    private String cardTitle = "//div[@class='title']/h3[normalize-space(text()='%s')]";
    private By inputNewCardTitle = By.xpath("//input[@class='changeTitle']");
    private By inputTaskCurrentName = By.xpath("//li/input");
    private String cardTrash = cardTitle + "//i[@class='fas fa-trash']";
    private String taskTrash = task + "//i[@class='fas fa-trash']";

    public DashboardPage() {
        setDefaultBaseUrl("https://mighty-garden-74991.herokuapp.com/api/login");
    }

    public String getNavbarHeaderText() {
        return find(navbarHeader).getText();
    }
    public String  getNavbarSignOutText() {
        return find(navbarSignOut).getText();
    }
    public boolean isNewCardBtnDisplayed() {
        return find(newCardBtn).isDisplayed();
    }

    public boolean isNewCardDisplayed() throws InterruptedException {
        Thread.sleep(300);
        return find(card).isDisplayed();
    }

    public boolean isNewTaskDisplayed(String taskName) {
        return find(String.format(task, taskName)).isDisplayed();
    }

    public boolean isNewCardTitleDisplayed(String newCardTitle) {
        return find(String.format(cardTitle, newCardTitle)).isDisplayed();
    }

    public boolean isCardPresent(String cardName) throws InterruptedException {
        Thread.sleep(300);
        return find(String.format(cardTitle, cardName)).isPresent();
    }

    public boolean isTaskPresent(String taskName) throws InterruptedException {
        Thread.sleep(300);
        return find(String.format(task, taskName)).isPresent();
    }

    public boolean isCurrentTaskRenamed(String newTask) {
        return find(String.format(task, newTask)).isDisplayed();
    }

    public boolean isNewTaskChangedStatus(String taskName) {
        return find(String.format(task, taskName)).isDisplayed();
    }

    public LoginPage clickOnNavbarSignOut() {
        find(navbarSignOut).click();
        return new LoginPage();
    }

    public DashboardPage clickOnNewCardBtn() {
        find(newCardBtn).click();
        return this;
    }

    public DashboardPage typeNewTaskAndConfirm(String task) {
        find(inputTask).typeAndEnter(task);
        return this;
    }

    public DashboardPage changeTaskStatus(String newTask) {
        find(String.format(task, newTask)).click();
        return this;
    }

    public DashboardPage doubleClickOnCardTitle(WebDriver driver) throws InterruptedException {

        WebElement cardTitle = find(cardTitleElement);
        new Actions(driver).doubleClick(cardTitle).build().perform();
        return this;
    }

    public DashboardPage doubleClickOnTask(WebDriver driver, String newTask) {
        WebElement taskEl = find(String.format(task, newTask));
        new Actions(driver).doubleClick(taskEl).build().perform();
        return this;
    }

    public DashboardPage typeCardTitle(String cardTitle) {
        find(inputNewCardTitle).typeAndEnter(cardTitle);
        return this;
    }

    public DashboardPage typeTask(String newTask) {
        find(inputTaskCurrentName).clear();
        find(inputTaskCurrentName).typeAndEnter(newTask);
        return this;
    }

    public DashboardPage hoverOnCardTitle(String cardName, WebDriver driver) {
        WebElement title = find(String.format(cardTitle, cardName));
        new Actions(driver).moveToElement(title).build().perform();
        return this;
    }

    public DashboardPage hoverOnTask(String taskName) {
        WebElement taskTitle = find(String.format(task, taskName));
        new Actions(getDriver()).moveToElement(taskTitle).build().perform();
        return this;
    }

    public DashboardPage removeTask(String taskName) throws InterruptedException {
        Thread.sleep(1000);
        find(String.format(taskTrash, taskName)).click();
        return this;
    }

    public DashboardPage removeCard(String cardName) throws InterruptedException {
        Thread.sleep(1000);
        find(String.format(cardTrash, cardName)).click();
        return this;
    }
}
